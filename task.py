import random


def enter_num():
    while True:
        try:
            var = int(input("Сколько камней вы хотите взять? \n"))
            break
        except ValueError:
            print("Значение некорректно.\n")
    return var


def main():
    total_stones = random.randint(4, 30)
    stones_count = total_stones
    user_turn = 0

    print(f"В начале игры у нас есть: {stones_count} камней.\n")

    while stones_count > 0:
        user_turn += 1

        print(f"Ваш ход: {user_turn}. Всего камней осталось {stones_count}.\n")

        while True:
            take_stones = enter_num()
            if not (1 <= take_stones <= (stones_count if (stones_count < 3) else 3)):  #
                #.khk
                print("Число не подходит.\n")
                continue
            break
        print(f"Вы взяли {take_stones} камней.\n")
        stones_count -= take_stones

        if stones_count <= 0:
            print("Вы выиграли!\n")
            break

        computer_take_stones = random.randint(1, 3)
        computer_turn = user_turn
        stones_count -= computer_take_stones
        print(f"Компьютер взял {computer_take_stones} камней на {computer_turn} ходу.\n")

        if stones_count <= 0:  #
            print("Вы проиграли!\n")
            break


main()
